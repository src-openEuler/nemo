Name:           nemo
Summary:        File manager for Cinnamon
Version:        5.6.2
Release:        2
License:        GPLv2+ and LGPLv2+
URL:            https://github.com/linuxmint/%{name}
Source0:        https://github.com/linuxmint/%{name}/archive/%{version}/%{name}-%{version}.tar.gz
Source1:        nemo.gschema.override
Patch0:         remove_desktop_search.patch
Patch1:         Don-t-scale-text-size-when-zooming.patch
Patch2:         allow_root_running.patch

ExcludeArch:    %{ix86}

BuildRequires:  meson
BuildRequires:  intltool
BuildRequires:  python3-gobject-base
BuildRequires:  desktop-file-utils
BuildRequires:  pkgconfig(libxml-2.0)
BuildRequires:  pkgconfig(cinnamon-desktop) >= 5.6.0
BuildRequires:  pkgconfig(sm)
BuildRequires:  pkgconfig(libexif)
BuildRequires:  pkgconfig(libgsf-1)
BuildRequires:  pkgconfig(exempi-2.0)
BuildRequires:  pkgconfig(libselinux)
BuildRequires:  pkgconfig(gobject-introspection-1.0)
BuildRequires:  pkgconfig(libnotify)
BuildRequires:  pkgconfig(xapp) >= 2.2.0

Requires:       openEuler-menus
Requires:       gvfs%{?_isa}
Requires:       gvfs-goa
Requires:       xapps%{?_isa} >= 2.2.0
# required for for gtk-stock fallback
Requires:       adwaita-icon-theme
Requires:       cinnamon-translations >= 5.6.0
Recommends:     nemo-search-helpers

# the main binary links against libnemo-extension.so
# don't depend on soname, rather on exact version
Requires:       nemo-extensions%{?_isa} = %{version}-%{release}

%description
Nemo is the file manager and graphical shell for the Cinnamon desktop
that makes it easy to manage your files and the rest of your system.
It allows to browse directories on local and remote filesystems, preview
files and launch applications associated with them.
It is also responsible for handling the icons on the Cinnamon desktop.

%package    extensions
Summary:    Nemo extensions library
License:    LGPLv2+
Requires:   %{name}%{?_isa} = %{version}-%{release}

%description extensions
This package provides the libraries used by nemo extensions.

%package    search-helpers
Summary:    Nemo search helpers
License:    LGPLv2+
Requires:   %{name}%{?_isa} = %{version}-%{release}
#Requires:   exif
Requires:   ghostscript
#Requires:   odt2txt
Requires:   poppler-utils
Requires:   python3-xlrd

%description search-helpers
This package provides the search helpers used by nemo.

%package    devel
Summary:    Support for developing nemo extensions
License:    LGPLv2+
Requires:   %{name}%{?_isa} = %{version}-%{release}
Requires:   %{name}-extensions%{?_isa} = %{version}-%{release}

%description devel
This package provides libraries and header files needed
for developing nemo extensions.

%prep
%autosetup -p1
# set treat-root-as-normal to true
sed -i '420s/false/true/' libnemo-private/org.nemo.gschema.xml

%build
%meson \
  -D deprecated_warnings=false \
  -D gtk_doc=false \
  -D selinux=true
%meson_build

%install
%meson_install

install -D -m 0644 %{SOURCE1} %{buildroot}/%{_datadir}/glib-2.0/schemas/nemo-fedora.gschema.override

desktop-file-install --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --add-only-show-in X-Cinnamon \
  %{buildroot}%{_datadir}/applications/*

# create extensions directoy
mkdir -p %{buildroot}%{_libdir}/nemo/extensions-3.0/

rm %{buildroot}%{_datadir}/nemo/search-helpers/id3.nemo_search_helper
rm %{buildroot}%{_datadir}/nemo/search-helpers/pdf2txt.nemo_search_helper

%ldconfig_scriptlets extensions

%files
%doc AUTHORS NEWS
%license COPYING COPYING-DOCS
%{_bindir}/nemo
%{_bindir}/nemo-autorun-software
%{_bindir}/nemo-connect-server
%{_bindir}/nemo-desktop
%{_bindir}/nemo-open-with
%{_libexecdir}/nemo-*
%{_datadir}/nemo/actions/
%{_datadir}/nemo/icons/
%{_datadir}/nemo/script-info.md
%{_datadir}/applications/*
%{_datadir}/mime/packages/nemo.xml
%{_datadir}/icons/hicolor/*/*/*.png
%{_datadir}/icons/hicolor/scalable/*/*.svg
%{_datadir}/dbus-1/services/nemo*
%{_datadir}/glib-2.0/schemas/*
%{_datadir}/polkit-1/actions/org.nemo.root.policy
%{_datadir}/gtksourceview-*/language-specs/nemo_*.lang
%{_mandir}/man1/nemo*


%files extensions
%license COPYING.EXTENSIONS COPYING.LIB
%{_libdir}/libnemo-extension.so.*
%{_libdir}/nemo/
%{_libdir}/girepository-1.0/Nemo-3.0.typelib

%files search-helpers
%{_bindir}/nemo-mso-to-txt
%exclude %{_bindir}/nemo-ppt-to-txt
%{_bindir}/nemo-xls-to-txt
%exclude %{_bindir}/nemo-epub2text
# {_datadir}/nemo/search-helpers/
%exclude %{_datadir}/nemo/search-helpers/epub2text.nemo_search_helper
%exclude %{_datadir}/nemo/search-helpers/exif.nemo_search_helper
%exclude %{_datadir}/nemo/search-helpers/libreoffice.nemo_search_helper
%exclude %{_datadir}/nemo/search-helpers/mso-doc.nemo_search_helper
%exclude %{_datadir}/nemo/search-helpers/mso-ppt.nemo_search_helper
%{_datadir}/nemo/search-helpers/mso-xls.nemo_search_helper
%{_datadir}/nemo/search-helpers/mso.nemo_search_helper
%{_datadir}/nemo/search-helpers/pdftotext.nemo_search_helper
%{_datadir}/nemo/search-helpers/ps2ascii.nemo_search_helper
%exclude %{_datadir}/nemo/search-helpers/untex.nemo_search_helper


%files devel
%{_includedir}/nemo/
%{_libdir}/pkgconfig/*
%{_libdir}/*.so
%{_datadir}/gir-1.0/*.gir

%changelog
* Tue Mar 14 2023 wenlong ding Mwenlong.ding@turbolinux.com.cn> - 5.6.2-2
- Add missing Patch2
- Adaptor to openEuler

* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 5.6.2-1
- update to 5.6.2

* Fri May 6 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 5.2.3-1
- Initial Packaging
